const minimist = require('minimist');
const fs = require('fs');
const path = require('path');
const flatten = require('flat');

console.log('Parsing i18n files...');

process.on('uncaughtException', error => {
	console.error(error);
	process.exit(1);
});

function read(path) {
	return flatten(JSON.parse(fs.readFileSync(path)));
}

function save(path, content) {
	fs.writeFileSync(
		path,
		JSON.stringify(flatten.unflatten(content), null, 2) + '\n',
	);
}

function getOrderedObject(unordered) {
	return Object.keys(unordered).sort().reduce(
		(obj, key) => {
			obj[key] = unordered[key];
			return obj;
		},
		{},
	);
}

// Build args.
const args = minimist(process.argv.slice(2));
const assetsPath = path.resolve(args.path || './src/assets/i18n');
const templateFileName = args.template || 'template.json';
const fixMode = args.f === true;
const placeholder = args.placeholder || '__PLACEHOLDER__';

let filenames = fs.readdirSync(assetsPath);

if (!filenames.includes(templateFileName)) {
	throw new Error('Template file is missing in ' + assetsPath);
}

filenames = filenames.filter(file => file !== templateFileName);

if (filenames.length === 0) {
	throw new Error('Directory exists but there is no i18n files');
}

console.log(`Found ${filenames.length} files...`);

const template = read(path.join(assetsPath, templateFileName));

if (fixMode) {
	save(path.join(assetsPath, templateFileName), getOrderedObject(template));
}

let errorCount = 0;
let warningCount = 0;

filenames.forEach(filename => {
	const file = read(path.join(assetsPath, filename));

	// Missing keys.
	Object.keys(template).forEach(key => {
		if (!file.hasOwnProperty(key)) {
			if (fixMode) {
				file[key] = template[key].length > 0
					? `__${template[key]}__`
					: placeholder;
			} else {
				console.log(
					'\n',
					'\x1b[4m' + path.join(assetsPath, filename) + '\x1b[0m',
					'\n',
					'  \x1b[31merror\x1b[0m',
					`You must add the "${key}" key in this file.`,
					'\x1b[2mmissing key\x1b[0m',
				);
			}

			errorCount++;
		}
	});

	// Extra keys and missing translations.
	Object.keys(file).forEach(key => {
		if (!template.hasOwnProperty(key)) {
			if (fixMode) {
				delete file[key];
			} else {
				console.log(
					'\n',
					'\x1b[4m' + path.join(assetsPath, filename) + '\x1b[0m',
					'\n',
					'  \x1b[31merror\x1b[0m',
					`You must remove the "${key}" key from this file.`,
					'\x1b[2mextra key\x1b[0m',
				);
			}

			errorCount++;
		} else if (!fixMode && file[key].startsWith('__') && file[key].endsWith('__')) {
			console.log(
				'\n',
				'\x1b[4m' + path.join(assetsPath, filename) + '\x1b[0m',
				'\n',
				'  \x1b[33mwarn\x1b[0m',
				`You must translate the "${key}" key in this file.`,
				'\x1b[2mmissing translation\x1b[0m',
			);

			warningCount++;
		}
	});

	if (fixMode) {
		save(path.join(assetsPath, filename), getOrderedObject(file));
	}
});

if (fixMode) {
	console.log(
		'\n',
		`✖ ${errorCount + warningCount} problems fixed`,
	);
} else if ((errorCount + warningCount) > 0) {
	const color = errorCount > 0 ? '31' : '33';
	console.log(
		'\n',
		`\x1b[${color}m✖ ${errorCount + warningCount} problems\x1b[0m`,
		`(\x1b[31m${errorCount} error(s)\x1b[0m, \x1b[33m${warningCount} warning(s)\x1b[0m)`,
		'\n\n',
		'To address some issues, run \x1b[2mnpm run i18n:fix\x1b[0m',
	);

	if (errorCount > 0) {
		process.exit(1);
	}
} else {
	console.log('\n', 'All files has been validated.');
}

process.exit();
