import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FenlistComponent } from './fenlist.component';

describe('FenlistComponent', () => {
  let component: FenlistComponent;
  let fixture: ComponentFixture<FenlistComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [FenlistComponent]
    });
    fixture = TestBed.createComponent(FenlistComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
