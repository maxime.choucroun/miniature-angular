import { Component , OnInit, OnChanges, SimpleChanges, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, FormsModule, Validators, FormControl, FormGroup } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { NgIf } from '@angular/common';  

import {AccountService} from './../../services/account.service'
import { User } from './../../modeles/user';

/* https://angular.io/guide/reactive-forms */
/* https://www.telerik.com/blogs/angular-basics-what-reactive-forms-when-use-them */

/*  https://www.digitalocean.com/community/tutorials/angular-reactive-forms-introduction validation a faire */


@Component({
  standalone: true,
  selector: 'app-creationnouveaucompte',
  templateUrl: './creationnouveaucompte.component.html',
  styleUrls: ['./creationnouveaucompte.component.css'],
  imports: [ReactiveFormsModule, NgIf],
})

export class CreationnouveaucompteComponent {

  @Input()  bFormFen = false;
  //pattern = '/^([1-8PNBRQK]+\/){7}[1-8PNBRQK]+$/gim'; 
  pattern = '^([1-8PNBRQKpnbrqk]{1,8}\/){1,8}([1-8PNBRQKpnbrqk]{1,8})?$';
  /*  pour checking
         8/8/4P1K1/2K1P3/8/PK/8
         rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR
  */

  submitted = false;
  //rapportInsertion = '';
  //boolRapportInsertion = false;

  public contactForm : FormGroup = new FormGroup({
    lastName: new FormControl("", [Validators.required]),
    firstName: new FormControl(""),
    email: new FormControl("",[Validators.required,Validators.pattern("^[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$")]),
    password: new FormControl(""),
    elo: new FormControl("",[Validators.min(1000), Validators.max(3000),Validators.pattern('[1-9]*')])
  });

  constructor(private formbuilder: FormBuilder , private accountservice:AccountService) {}

  onSubmit() {
    user: User;

    console.log('lastname:', this.contactForm.get('lastName'));
   
    console.log('lastname:', this.contactForm.value.lastName);
    console.log('firstname:', this.contactForm.value.firstName);
    console.log('email:', this.contactForm.value.email);
    console.log('password:', this.contactForm.value.password);
    console.log('elo:', this.contactForm.value.elo);  
    
   const user = { ...this.contactForm.value }

    this.accountservice.register(user);

/*

    export class User {
      id?: string;
      username?: string;
      password?: string;
      firstName?: string;
      lastName?: string;
      token?: string;
    }
*/


    this.submitted = true;

    if (this.contactForm.invalid) { return; }





  }
}

