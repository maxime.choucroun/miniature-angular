import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CreationnouveaucompteComponent } from './creationnouveaucompte.component';

describe('CreationnouveaucompteComponent', () => {
  let component: CreationnouveaucompteComponent;
  let fixture: ComponentFixture<CreationnouveaucompteComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CreationnouveaucompteComponent]
    });
    fixture = TestBed.createComponent(CreationnouveaucompteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
