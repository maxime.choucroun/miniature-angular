import {NgModule} from '@angular/core';

import { FenlistComponent } from './form/fenlist/fenlist.component';
import { ConnexionComponent } from './form/connexion/connexion.component';
import { HomepageComponent } from './form/homepage/homepage.component';
import { CreationnouveaucompteComponent } from './form/creationnouveaucompte/creationnouveaucompte.component';

 

import {RouterModule,Routes} from '@angular/router';

export const appRouteList: Routes = [
	{ path: '',	         component: HomepageComponent   },
	{	path: 'fenlist',   component: FenlistComponent  	},
	{	path: 'connexion', component: ConnexionComponent	},
	{	path: 'homepage',	 component: HomepageComponent   },
	{	path: 'creationnouveaucompte',	 component: CreationnouveaucompteComponent },
	{	path: '**',   		 redirectTo: 'homepage'       	}
];

@NgModule({
	imports: [RouterModule.forRoot(appRouteList)],
	exports: [RouterModule],
})
export class AppRoutingModule { }
