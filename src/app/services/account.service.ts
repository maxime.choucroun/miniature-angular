import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { User } from './../modeles/user';

import axios from 'axios';


@Injectable({ providedIn: 'root' })
export class AccountService {
    //@ts-ignore 
    public user: User;
    constructor(   ) {  }

    public get userValue() {
        return this.user;
    }

    login(username: string, password: string) {
       //return this.http.post<User>(`${environment.apiUrl}/users/authenticate`, { username, password })
    }

    logout() {
        // remove user from local storage and set current user to null
        localStorage.removeItem('user');
       // this.router.navigate(['/account/login']);
    }

    async register(user: User) {
        console.log(`${environment.apiUrl}/users/register`) 
        console.log(user)  

        axios.request({
            method: 'POST',
            data: JSON.stringify(user),
            url: `${environment.apiUrl}/users/register`
        }) 
    }

    getAll() {
        //return this.http.get<User[]>(`${environment.apiUrl}/users`);
    }

    getById(id: string) {
        //return this.http.get<User>(`${environment.apiUrl}/users/${id}`);
    }

    update(id: string, params: any) {

    }

    delete(id: string) {
    }
}