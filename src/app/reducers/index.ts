import {isDevMode} from '@angular/core';
import {ActionReducerMap, MetaReducer} from '@ngrx/store';

export type State = Record<string, unknown>;

export const reducers: ActionReducerMap<State> = {

};

export const metaReducers: Array<MetaReducer<State>> = isDevMode() ? [] : [];
