import {isDevMode, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {StoreModule} from '@ngrx/store';
import {TemplateCardComponent} from './features/template/components/template-card/template-card.component';
import {TemplateCmdComponent} from './features/template/components/template-cmd/template-cmd.component';
import {reducers, metaReducers} from './reducers';
import {EffectsModule} from '@ngrx/effects';
import {StoreRouterConnectingModule} from '@ngrx/router-store';
import {StoreDevtoolsModule} from '@ngrx/store-devtools';
//import {HttpClientModule} from '@angular/common/http';
import {TranslocoRootModule} from './transloco-root.module';

import {MatButtonModule} from '@angular/material/button';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatIconModule} from '@angular/material/icon';
import {MatBadgeModule} from '@angular/material/badge';
import {MatTableModule} from '@angular/material/table';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatInputModule} from '@angular/material/input';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';

import { ReactiveFormsModule } from '@angular/forms';

import {TopComponent} from './top/top.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';


@NgModule({
	declarations: [
		AppComponent,
		TopComponent,
	],
	imports: [
		BrowserModule,
		AppRoutingModule,
		StoreModule.forRoot(reducers, {metaReducers}),
		EffectsModule.forRoot([]),
		StoreRouterConnectingModule.forRoot(),
		StoreDevtoolsModule.instrument({
			maxAge: 100,
			logOnly: !isDevMode(),
			autoPause: true,
			name: 'Angular Starter. Please rename me.',
		}),
		/*HttpClientModule,*/
		TranslocoRootModule,
		TemplateCardComponent,
		TemplateCmdComponent,
		BrowserAnimationsModule,
		MatButtonModule,
		MatToolbarModule,
		MatIconModule,
		MatBadgeModule,
		MatTableModule,
		MatCheckboxModule,
		MatFormFieldModule,
		MatInputModule,
		MatSlideToggleModule,
		ReactiveFormsModule,
	],
	providers: [],
	bootstrap: [AppComponent],
})
export class AppModule { }
