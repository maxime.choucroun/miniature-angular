import {HttpClient} from '@angular/common/http';
import {Translation, TranslocoLoader} from '@ngneat/transloco';
import {TRANSLOCO_LOADER, TRANSLOCO_CONFIG, translocoConfig, TranslocoModule} from '@ngneat/transloco';
import {Injectable, isDevMode, NgModule} from '@angular/core';

@Injectable({providedIn: 'root'})
export class TranslocoHttpLoader implements TranslocoLoader {
	constructor(private readonly http: HttpClient) {}

	getTranslation(lang: string) {
		return this.http.get<Translation>(`/assets/i18n/${lang}.json`);
	}
}

@NgModule({
	exports: [TranslocoModule],
	providers: [
		{
			provide: TRANSLOCO_CONFIG,
			useValue: translocoConfig({
				availableLangs: ['fr', 'en'],
				defaultLang: 'fr',
				// Remove this option if your application doesn't support changing language in runtime.
				reRenderOnLangChange: true,
				prodMode: !isDevMode(),
			}),
		},
		{provide: TRANSLOCO_LOADER, useClass: TranslocoHttpLoader},
	],
})
export class TranslocoRootModule {}
