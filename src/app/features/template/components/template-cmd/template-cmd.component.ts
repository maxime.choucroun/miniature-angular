import {ChangeDetectionStrategy, Component, EventEmitter, Input, Output} from '@angular/core';

@Component({
	selector: 'app-template-cmd',
	standalone: true,
	templateUrl: './template-cmd.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TemplateCmdComponent {
	@Input() title = '';
	@Output() selected = new EventEmitter<true>();

	select(): void {
		this.selected.emit(true);
	}
}
