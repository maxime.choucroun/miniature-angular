import {ChangeDetectionStrategy, Component, Input} from '@angular/core';

@Component({
	selector: 'app-template-card',
	standalone: true,
	templateUrl: './template-card.component.html',
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TemplateCardComponent {
	@Input() href = '';
	@Input() title = '';
}
