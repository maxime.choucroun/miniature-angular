import {Component, ChangeDetectionStrategy} from '@angular/core';


@Component({
	selector: 'app-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.css', './../styles/common.css'],
	changeDetection: ChangeDetectionStrategy.OnPush,
	providers: []})

export class AppComponent {
	title = 'Chess-Miniature';
	lastAction = '';

	affichage(type: string, formToVisible: string) {
		console.log('type: %s', type);
		console.log('formToVisible: %s', formToVisible);
	}
}
