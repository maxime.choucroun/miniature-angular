module.exports = {
    root: true,
    ignorePatterns: ['*.html'],
    env: {
        browser: true,
        jasmine: true,
        node: true,
    },
    extends: 'xo',
    overrides: [{
        files: ['*.ts', '*.tsx'],
        parserOptions: {
            project: ['tsconfig.lint.json'],
            createDefaultProgram: true,
        },
        extends: [
            'xo-typescript',
            'plugin:@angular-eslint/recommended',
        ],
        rules: {
            // Authorize Angular decorators.
            'new-cap': [
                'error',
                {
                    capIsNew: true,
                    allowIndentationTabs: true,
                    capIsNewExceptions: [
                        'Component',
                        'Directive',
                        'HostBinding',
                        'HostListener',
                        'Inject',
                        'Injectable',
                        'Input',
                        'NgModule',
                        'Output',
                        'Pipe',
                        'ViewChild',
                        'ViewChildren',
                    ],
                    newIsCap: true,
                    properties: true,
                },
            ],
            // Type imports doesn't work with some Angular modules.
            '@typescript-eslint/consistent-type-imports': ['error', { prefer: 'no-type-imports' }],
            // Custom naming convention.
            '@typescript-eslint/naming-convention': [
                'error',
                {
                    selector: 'default',
                    format: ['camelCase'],
                    leadingUnderscore: 'forbid',
                },
                {
                    selector: 'variable',
                    format: ['camelCase', 'UPPER_CASE'],
                },
                {
                    selector: 'typeLike',
                    format: ['PascalCase'],
                },
                // Compatibility with NgRx createActionGroup().
                {
                    selector: 'objectLiteralProperty',
                    format: null,
                },
            ],
            // Strict configuration for unused vars.
            '@typescript-eslint/no-unused-vars': ['error'],
            // Compatibility with the Angular router.
            '@typescript-eslint/promise-function-async': 'off',
            '@angular-eslint/component-selector': ['error', { prefix: 'app' }],
            '@angular-eslint/contextual-decorator': 'error',
            '@angular-eslint/directive-selector': [
                'error',
                {
                    type: 'attribute',
                    prefix: 'app',
                    style: 'camelCase',
                },
            ],
            '@angular-eslint/no-attribute-decorator': 'error',
            '@angular-eslint/no-lifecycle-call': 'error',
            '@angular-eslint/no-pipe-impure': 'error',
            '@angular-eslint/prefer-on-push-component-change-detection': 'error',
        },
    }, ],
    parserOptions: {
        ecmaVersion: 'latest',
        sourceType: 'module',
    },
};