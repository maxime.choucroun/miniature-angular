# Sets the base image
FROM node:18

# Sets the working directory to /app in the container
WORKDIR /app

# Copies package.json and tsconfig.json to the container
COPY package*.json ./
COPY tsconfig.json ./

# Clean Install (ci) the packages from package.json
RUN npm ci

# Copies the source code to the container
COPY . .

# Builds the application using the build script defined in package.json
RUN npm install -g @angular/cli
RUN npm run build
RUN ls
# Copies the environment variables file to the container
COPY .env .

EXPOSE 80
EXPOSE 4200

# Sets the entrypoint command to run the application
CMD ["ng", "serve", "--host", "0.0.0.0", "--port", "4200"]