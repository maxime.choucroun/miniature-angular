# Starter

Ce projet est un starter Angular. Il permet de mettre en place rapidement une stack fonctionnelle et complète.

Ce projet contient :
* le framework Angular préconfiguré,
* le framework NgRx préconfiguré,
* une librairie d'internationalisation préconfigurée,
* une librairie de lint préconfigurée,
* et quelques détails supplémentaires...

Les différents choix effectués sont détaillés dans la suite de ce fichier. Il est recommandé de le lire entièrement avant de modifier ou de copier ce projet.

## Stack & choix techniques

### Angular

Ce starter utilise une version simple d'Angular (router inclus) sans modification particulière.

### Styles

Nous avons choisi d'utiliser **un dossier dédié**. Nous vous recommandons **de ne pas lier le style à vos composants.**

> Dans la plupart de nos projets, l'intégration est effectué par une équipe (ou une entreprise) externe. Isoler le style permet de limiter les conflits et de faciliter les mises-à-jour.

Le dossier asset est exposé tel quel (configuration par défaut).

### NgRx

[NgRx](https://ngrx.io/docs) est un framework inspiré de Redux pour construire des applications réactives.

> Maîtriser NgRx vous aidera à construire des applications Angular maintenables et scalable. Nous vous recommandons de l'utiliser quelle que soit la taille de votre application.

Dans ce starter, nous incluons les librairies suivantes :
* @ngrx/store
* @ngrx/effects
* @ngrx/router-store
* @ngrx/store-devtools
* @ngrx/eslint-plugin (voir la partie Linter).

Le framework NgRx propose davantage de librairie. Vous pouvez consulter la documentation officielle pour les découvrir.

> Nous vous conseillons d'attendre de maîtriser les bases de NgRx avant d'envisager l'ajout d'une librairie NgRx supplémentaire.

Vous devrez également installer le plugin "Redux Dev Tools" sur votre navigateur.

### Internationalisation

La librairie [Transloco](https://ngneat.github.io/transloco/docs/translation-in-the-template) est utilisée pour l'internationalisation. Nous vous recommandons de l'utiliser même si vous utilisez habituellement d'autres librairies afin de garantir une continuité entre les projets Jems.

> Transloco est une librairie simple, performante et activement maintenue.

Le format utilisé pour les fichiers de traduction est le JSON. C'est un format simple que des intervenants externes (et non-techniques) peuvent facilement modifier sans risque.

Ce projet fournit également un script (`i18n.js`) qui vous permet de manipuler et de valider vos fichiers de traduction :
* un fichier `template.json` indique les clefs que **tous vos fichiers** de traductions doivent contenir,
* le script `npm run i18n:fix` permet de mettre à jour automatiquement tous vos fichiers de traduction,
* le script `npm run i18n` permet de vérifier vos fichiers de traduction :
  * en déclenchant une erreur si une clef est en trop ou manquante,
  * en déclenchant un avertissement si une clef n'est pas encore traduite.

### Eslint

Eslint s'assure que la codebase respecte nos conventions de code. Vous pouvez configurer votre IDE pour qu'il applique automatiquement ces règles lorsque vous codez.

> L'utilisation automatisée (et intégrée à la CI) d'un linter a plusieurs avantages, notamment une amélioration à moindre coût de la qualité du code et une réduction du bruit lors des revues de code.

La configuration est déjà faite. Nous vous recommandons **de ne pas la modifier**, même si elle ne correspond pas à vos habitudes. Cela permet notamment d'harmoniser les différents projets.

Nous utilisons un ruleset populaire pour TypeScript (`xo`) et des règles supplémentaires (personnalisées) pour Angular et NgRx.

### Tests

@todo:
* configuration de Karma en headless
* commande de coverage
* CI

## Installation

### Préparation du projet

@todo:
* script JS pour faire le rename automatiquement

* Renommer le package (`/package.json`).
* Corriger les mentions au nom du package dans la configuration Angular (`/angular.json`).
* Choisissez un préfixe unique pour vos composants ( `projects.{your-project}.prefix` in `/angular.json`).
  * Vous pouvez utiliser le préfixe par défaut (`app`).
* Corriger le nom de l'application dans la configuration du StoreDevtoolsModule (`app.module.ts`).
* Corriger le préfixe Angular dans la configuration du linter (`eslintrc.js`).

### Installation

@todo:
* techno. requises
* branche dédié avec fichiers dockers (node 16 brocelia)
